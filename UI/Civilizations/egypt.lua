-- egypt obelisk border expansion
-- author: Ashwin
-- date: 12/16/2024

include("snowveutilities.lua")

local iCiv = GameInfoTypes["CIVILIZATION_EGYPT"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
local obeliskID = GameInfoTypes["BUILDING_BURIAL_TOMB"]
local iNumGrows = 2

function ObeliskBorderGrow(playerID, cityID, buildingID)
	if buildingID == obeliskID then
		local player = Players[playerID]
		local city = player:GetCityByID(cityID)
		for count = 1, iNumGrows do
			local nextPlot = city:GetNextBuyablePlot()
			nextPlot:SetOwner(playerID)
			nextPlot:SetCityPurchaseID(cityID)
		end
	end
end

if bIsActive then
	GameEvents.CityConstructed.Add(ObeliskBorderGrow)
end