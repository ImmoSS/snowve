-- map reveal options 
-- Author: Ashwin
-- Date: 11/15/24

include("snowveutilities.lua")

local bRevealWonders = Game.IsOption("GAMEOPTION_REVEAL_WONDERS")
local bRevealSides = Game.IsOption("GAMEOPTION_REVEAL_SIDES")
local bLuxuryCount = Game.IsOption("GAMEOPTION_LUXURY_COUNT")
local bPantheonCount = Game.IsOption("GAMEOPTION_PANTHEON_COUNT")
worldWonders = {7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20, 21, 22, 23, 24}
luxIDs = {13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 53, 56, 57,}
luxCount = {}
campIDs = {9, 19, 20, 33, 40}
campCount = 0
harvestIDs = {6, 55}
harvestCount = 0

function MapReveals()
	turn = Game.GetGameTurn()
	if turn < 10 then
		iMaxX = Map.GetWidth()
		iMaxY = Map.GetHeight()
		iTeam1 = Players[0]:GetTeam()
		iTeam2 = Players[1]:GetTeam()
		local pPlot
		for x = 0, iMaxX-1 do
			for y = 0, iMaxY-1 do
				pPlot = Map.GetPlot(x, y)
				if pPlot then
					if bRevealSides then
						revealPlot(pPlot, iTeam1, iTeam2, iMaxX)
					elseif bRevealWonders then
						revealWonder(pPlot, iTeam1, iTeam2, iMaxX)
					end
					if bLuxuryCount or bPantheonCount then
						local resourceID = pPlot:GetResourceType(-1)
						if bLuxuryCount then
							countLuxury(resourceID)
						end
						if bPantheonCount then
							countPantheon(resourceID)
						end
					end
				end
			end
		end
		if bLuxuryCount then
			luxuryNotification()
		end
		if bPantheonCount then
			pantheonNotification()
		end
	end
end

function revealPlot(pPlot, iTeam1, iTeam2, iMaxX)
	if (pPlot:GetX() <= iMaxX /2 and not (pPlot:GetTerrainType() == TerrainTypes.TERRAIN_SNOW)) then 
		pPlot:SetRevealed(iTeam1, true)
	elseif (pPlot:GetX() >= iMaxX / 2 and not (pPlot:GetTerrainType() == TerrainTypes.TERRAIN_SNOW)) then
		pPlot:SetRevealed(iTeam2, true)
	end
end

function revealWonder(pPlot, iTeam1, iTeam2, iMaxX)
	local featureID = pPlot:GetFeatureType()
	if array_has_value(worldWonders, featureID) then
		revealPlot(pPlot, iTeam1, iTeam2, iMaxX)
	end
end

function countLuxury(resourceID)
	if array_has_value(luxIDs, resourceID) then
		if luxCount[resourceID] == nil then
			luxCount[resourceID] = 0
		end
		luxCount[resourceID] = luxCount[resourceID] + 1
	end
end

function countPantheon(resourceID)
	if array_has_value(campIDs, resourceID) then
		campCount = campCount + 1
	end
	if array_has_value(harvestIDs, resourceID) then
		harvestCount = harvestCount + 1
	end
end

function luxuryNotification()
	local numLuxes = 0
	local numSingles = 0
	local numDupes = 0
	local numTrips = 0
	local numQuads = 0
	for lux, count in pairs(luxCount) do
		if count ~= 0 then 
			numLuxes = numLuxes + 1
			if count == 2 then
				numSingles = numSingles + 1
			elseif count == 4 then
				numDupes = numDupes + 1
			elseif count == 6 then 
				numTrips = numTrips + 1
			elseif count >= 8 then 
				numQuads = numQuads + 1
			end
		end
	end
	iTeam1 = Players[0]:GetTeam()
	local teamSize = 0
	for playerID, player in pairs(Players) do
		local player = Players[playerID]
		if player:GetTeam() == iTeam1 then
			teamSize = teamSize + 1
		end
	end
	for playerID, player in pairs(Players) do
		if teamSize == 1 then
			player:AddNotification(NotificationTypes.NOTIFICATION_GENERIC, string.format("There are %d unique luxuries on the map.", numLuxes), "Luxury Count", -1, -1, -1)
		elseif teamSize == 2 then
			player:AddNotification(NotificationTypes.NOTIFICATION_GENERIC, string.format("There are %d unique luxuries on the map, with %d singletons and %d (at least) duplicates.", numLuxes, numSingles, numDupes + numTrips + numQuads), "Luxury Count", -1, -1, -1)
		elseif teamSize == 3 then
			player:AddNotification(NotificationTypes.NOTIFICATION_GENERIC, string.format("There are %d unique luxuries on the map, with %d singletons, %d duplicates, and %d (at least) triplicates.", numLuxes, numSingles, numDupes, numTrips + numQuads), "Luxury Count", -1, -1, -1)
		else
			player:AddNotification(NotificationTypes.NOTIFICATION_GENERIC, string.format("There are %d unique luxuries on the map, with %d singletons, %d duplicates, and %d triplicates, and %d (at least) quadruplicates.", numLuxes, numSingles, numDupes, numTrips, numQuads), "Luxury Count", -1, -1, -1)
		end
	end
end

function pantheonNotification()
	for playerID, player in pairs(Players) do
		player:AddNotification(NotificationTypes.NOTIFICATION_GENERIC, string.format("There are %d Goddess of the Hunt tiles and %d Harvest Festival tiles on the map.", campCount / 2, harvestCount / 2), "Pantheon Count", -1, -1, -1)
	end
end

if (bRevealWonders or bRevealSides or bLuxuryCount or bPantheonCount) then 
	Events.SequenceGameInitComplete.Add(MapReveals)
end